# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160704143214) do

  create_table "orders", force: :cascade do |t|
    t.string   "name"
    t.integer  "amount"
    t.string   "adress"
    t.string   "status"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "cpf",        default: ""
  end

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.float    "price"
    t.text     "ingredients"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "routines", force: :cascade do |t|
    t.string   "name"
    t.time     "hour"
    t.string   "product"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "cpf",        default: ""
    t.datetime "hourIn"
    t.datetime "hourOut"
    t.integer  "OrderID"
    t.integer  "status"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "name",                   default: ""
    t.string   "phone",                  default: ""
    t.string   "address",                default: ""
    t.string   "cpf",                    default: ""
    t.string   "rg",                     default: ""
    t.string   "workPermit",             default: ""
    t.date     "born"
    t.date     "contractDate"
    t.integer  "role",                   default: 0
    t.float    "workingload"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
