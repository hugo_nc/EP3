class AddStatusToRoutine < ActiveRecord::Migration
  def change
    add_column :routines, :status, :integer
  end
end
