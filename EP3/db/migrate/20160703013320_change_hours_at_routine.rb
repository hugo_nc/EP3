class ChangeHoursAtRoutine < ActiveRecord::Migration
  def change
    change_column :routines, :hourIn, :datetime
    change_column :routines, :hourOut, :datetime
  end
end
