class AddColumnsToRoutine < ActiveRecord::Migration
  def change
    add_column :routines, :cpf, :string, :default =>''
    add_column :routines, :hourIn, :time, :default =>''
    add_column :routines, :hourOut, :time, :default =>''
    add_column :routines, :OrderID, :integer, :default =>''
  end
end
