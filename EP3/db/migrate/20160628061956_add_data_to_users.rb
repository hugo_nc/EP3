class AddDataToUsers < ActiveRecord::Migration
  def change
    add_column :users, :name, :string, :default =>''
    add_column :users, :phone, :string, :default =>''
    add_column :users, :address, :string, :default =>''
    add_column :users, :cpf, :string, :default =>''
    add_column :users, :rg, :string, :default =>''
    add_column :users, :workPermit, :string, :default =>''
    add_column :users, :born, :date, :default =>''
    add_column :users, :contractDate, :date, :default =>''
    add_column :users, :role, :integer, :default =>0
  end
end
