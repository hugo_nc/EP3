json.array!(@routines) do |routine|
  json.extract! routine, :id, :name, :hour, :product
  json.url routine_url(routine, format: :json)
end
