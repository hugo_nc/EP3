class ControllUserController < ApplicationController
  before_action :authenticate_user!
  def index
    @users = User.all
    authorize @users
  end

  def analise
    @users = User.all
    @routines = Routine.all
    authorize @users
    authorize @routines
  end
end
