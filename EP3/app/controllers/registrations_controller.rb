class RegistrationsController < Devise::RegistrationsController
  before_action :authenticate_user!, :redirect_unless_admin,  only: [:new, :create]
  skip_before_action :require_no_authentication
  before_filter :configure_permitted_parameters, :only => [:create]

  def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name,:phone,:address,:cpf,:rg,:workPermit,
        :born,:contractDate,:role, :email, :password, :password_confirmation) }
    end

  private
  def redirect_unless_admin
    unless (current_user.try(:admin?)) or (current_user.nil?)
      flash[:notice] = "Somente administradores tem essa permissão!"
      redirect_to account_manager_index_path || root_path
    end
  end
  def sign_up(resource_name, resource)
    true
    if current_user.try(:admin?)
      @user = User.last
      @user.role=1
      @user.save
    end
  end

  def after_sign_in_path_for(resource_or_scope)
    account_manager_index_path
  end

end
