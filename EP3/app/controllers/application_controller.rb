class ApplicationController < ActionController::Base
  #Authorization based on roles
  include Pundit

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  before_filter :list
  def list
      @products = Product.all
  end

private
  def user_not_authorized
    flash[:notice] = "Você não tem permissão para fazer esta ação."
    redirect_to(account_manager_index_path)
  end

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # saves the location before loading each page so we can return to the
# right page. If we're on a devise page, we don't want to store that as the
# place to return to (for example, we don't want to return to the sign in page
# after signing in), which is what the :unless prevents
  def after_sign_in_path_for(resource_or_scope)
    account_manager_index_path
  end



end
