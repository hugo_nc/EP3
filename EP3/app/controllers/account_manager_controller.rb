class AccountManagerController < ApplicationController
  before_action :authenticate_user!

  def index
  end

  def emp_activity
    @orders = Order.all
    @routines = Routine.all
    authorize @orders
    authorize @routines
  end

  def emp_entry_time
    @routine = Routine.new
    @routine.name = current_user.name
    @routine.hourIn =Time.now
    @routine.cpf = current_user.cpf
    @routine.status = 1
    @routine.save
    authorize @routine
  end

  def emp_exit_time
    @routine = Routine.where(["cpf = :u", { u: current_user.cpf }]).last
    @routine.hourOut =Time.now
    @routine.status = 0
    @routine.save
    authorize @routine
  end

  def caixa
    @orders = Order.all
    @products = Product.all
    authorize @orders
    authorize @products
  end

end
