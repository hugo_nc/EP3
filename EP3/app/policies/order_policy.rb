class OrderPolicy < ApplicationPolicy

  def index?
      user.admin? or user.employee?
    end

  def new?
    user.admin? or user.employee?
  end

  def show?
    user.admin? or user.employee?
  end
  def edit?
    user.admin? or user.employee?
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
