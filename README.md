# O projeto   
SnackSystem é sistema intuitivo e de fácil útilização para o gerenciamento e controle de pedidos em um comércio do ramo alimentício.  
O projeto foi criado para a realização do exercício de programação 3 da disciplina de Orientação à Objetos do curso de Engenharia de Software da Universidade de Brasília Campus Gama.  
Para a execução do projeto foi utilizada a linguagem Ruby com o framework Rails e o ambiente de desenvolvimento foi o sistema operacional Linux.
Desenvolvido por: Hugo Neves de Carvalho <https://gitlab.com/u/hugo_nc> e João Paulo Nunes Soares< https://gitlab.com/u/jpnsoares >.   
O objetivo principal do projeto é criar um sistema simples e fácil de ser implementado e utilizado em pequenas lanchonentes ou pizzarias, que possa facilitar a integração e controle dos pedidos feitos.Além disso o sistema permite a realização de pedidos por meio dos clientes que estão fora do local ,dando a possibilidade deste acompanhar a etapa de produção em que seu pedido está.  
Dentre as funcionalidades presentes no sistema, podemos citar:  
            1)Cadastro de clientes  
            2)Cadastro de funcionários  
            3)Cadastro de produtos com sua respectiva imagem  
            4)Realização de Pedidos  
            5)Acompanhamento dos pedidos realizados por parte do cliente que realizou  
            6)Visualização de todos os pedidos feitos e controle de seu status  
            7)Controle de caixa por parte do administrador  
            8)Visualização do cardápio por qualquer pessoa  
            
# Configurando  
Antes de exectar a aplicação , verifique se possui ruby e o framework rails instalado em sua máquina.Se não possuir, instale e depois prossiga.  
    Caso com tudo instalado, realize os seguintes passos:  
 			1)Abra o terminal.  
 			2)Clone o repositório: ``` $ git clone https://gitlab.com/hugo_nc/EP3 ```   
 			3)Acesse o diretório em que a aplicação foi clonada  
 			4)Utilize o comando :```  $ bundle install ```  
 			5)Quando o bundle install finalizar as atualizações digite:```  $ rails server  ```   
 			6)Abra um browser de sua preferência e acesse:  http://localhost:3000/  
 			**OBS**: Caso não tenha o programa ImageMagick instalado em sua máquina, abra o terminal e digite o seguinte comando para instála-lo:  
 				```   
 				$ sudo apt-get install imagemagick
 				```   
 		Pronto, após estes passos você já terá acesso a página inicial do sistema.    
# Sobre os usuarios  
 O sistema possui 3 diferentes tipos de usuários:  
 * Administrador  
 * Funcionário  
 * Cliente    
 
 Todos cadastros realizados na página padrão são usuários comuns.  
 Os funcionários são cadastrados apenas pelo adminstrador.  
 Os administradores são cadastrados internamente no sistema, utilizando da própia console do Rails.  
 		Para cadastrar um usuário com permissões de Administrador siga os seguintes passos:  
 		**OBS:** Recomenda-se que o usuário admin seja o primeiro a realizar cadastro no site.  
            1)Na página da aplicação no Browser cadastre um novo usuário normal com as informações desejadas, este será o
 usuário Admin.  
 			2)Abra um novo terminal,diferente do terminal em que o server está funcionando, e acesse o diretório em que a aplicação está localizada.  
 			3)Digite: ```  $ rails c ```   
 			4)Após entrar no terminal da aplicação faça:  
 			``` 
 			    > x = User.first  
 			    > x.role = 2  
 				> x.save  
 			```   
 			5)Segure a tecla CTRL e aperte D ( CTRL+D) para fechar o terminal da aplicação.  
 			6)Pronto, faça login com este usuário e este terá as permissões de Administrador.

# Descrição de Classes  
Neste projeto foram criadas as seguintes classes:  

#### User   
Classe responsável pelos usuários da aplicação, nesta classe foram utilizadas gems Devise e Pundit.    
Os usuários respeitam o modelo CRUD, métodos de criação, requisição, atualização e destruição.  
A criação de um usuário no sistema ocorre depois que o usuário informa seus dados pessoais e escolhe uma senha. Esses dados variam de acordo com os níveis de prioridade.  
Os admins e usuários comuns possuem os mesmo, sendo diferente apenas seus **role**, já os funcionários devem informar mais alguns dados como: cpf, rg, carteira de trabalho, data de nascimento.  
A requisição ocorre apenas por parte do admin, que visualiza todos usuários de seu sistema.  
A iniciação no sistema também é importante, pois conforme o tipo for iniciado no sistema, algumas funções lhe serão apresentadas.  

#### Product  
Classe responsável pelo CRUD dos produtos, sendo que esta modelagem influencia na definição do métodos:
* Criação: Apenas os admins do sistema tem acesso, bastando que apenas sejam informados os atributos necessários,nome produto, preço ingredientes e uma foto caso deseje.  
* Requisição: A requisição dos dados cadastrados é comum para todos tipos de usuários, pois se faz necessário mostrar tanto para quem compra quanto para quem vende os produtos disponivéis.    
* Atualização: Ação apenas para o admin, que ao seu critério possa manter um cardápio rico de opções baseado no que seus clientes buscam.       
* Destruição: também somente acessível aos admins e funcionários, e importante para manutenção do cardápio.    
A criação dos produtos no sistema se faz necessária visto que os pedidos só poderam ser realizados se produtos forem setados (o campo de escolha de produtos é baseado na lista de todos produtos).  

#### Order  
Classe responsável pelo tratamento dos pedidos, desde sua criação no sistema até a sua destruição total.  
Utilizando do conceito CRUD possui métodos de:  
* Criação: todos usuários no sistema tem acesso, bastando apenas informar os atributos necessários,produto, quantidade e endereço.  
* Requisição: onde novamente todos podem visualizar o pedido realizado e neste ponto sendo informado qual o status do pedido.  
* Atualização: apenas visível para admins e funcionários, sendo que estes apenas alteram o status do pedido.   
* Destruição: também somente acessível aos admins e funcionários .    
Todos esses mecanismos são importantes para que toda a aplicação flua, pois não há sentido na existência da aplicação sem pedidos para serem tratados.  


#### Routine
Essa classe se preocupa em tratar rotinas de trabalho do funcionário, pois esse deve informar seu horário de entrada e saída e atividades realizadas.  
O horário de entrada é coletado diretamente do servidor, assim como o horário de sáida.  
A realização de atividades afeta tanto os funcionários quantos os pedidos por eles tratados, porque esta função que troca o status do pedido.  
A presença de um funcionário não é essencial, no fluxo do sistema, devido ao fato de que o administrador também consegue realizar as atividades nos pedidos.  

#### Além disso...
Além dessas classes criadas, foram geradas algumas controllers para melhor gerenciar interfaces.  
Algumas migrations para adicionar campos, antes não necessários e alterações necessárias em configs de rotas e outros arquivos para melhor funcionamento das gems.  

# Informações Técnicas
 ``` 
  			~> Versão Ruby: 2.3.1
 			~> Versão Rails: 4.2.6
 			~> Banco de Dados Utilizado: Sqlite3
 			~> Gem's utilizadas / Versão:
 			
 				-gem 'time_difference'          /
 				-gem 'twitter-bootstrap-rails'  /
 				-gem 'devise-bootstrap-views'   /
 				-gem 'bootstrap-sass'           /   3.2.0
 				-gem 'autoprefixer-rails'       /
 				-gem 'pundit'			        /
 				-gem 'devise'	              	/
 				-gem 'rails' 	              	/   4.2.6
 				-gem 'sqlite3'	             	/
 				-gem 'sass-rails'           	/   5.0
 				-gem 'uglifier'  	            /   >= 1.3.0
 				-gem 'coffee-rails' 		    /   4.1.0
 				-gem 'jquery-rails'		        /
 				-gem 'turbolinks'		        /
 				-gem 'jbuilder'   	            /   2.0
 				-gem 'sdoc' 			        /   0.4.0
 				-gem 'bcrypt'			        /   3.1.7
 			    -gem 'byebug'		            /
  				-gem 'web-console'   	        /   2.0
   		        -gem 'spring'			        /
  ```   
# **FAQ**
* Esta aplicação tem compatibilidade para quais Sistemas Operacionais?  
    O software apenas foi testado em ambiente Linux. Sendo assim, o seu desempenho em outros SO's é desconhecido.   
    É livre o teste em outros Sistemas Operacionais.
* É necessário importar alguma biblioteca para execução?  
    Não. Apenas as definidas por este tutorial.  
* A quem reportar em caso de problemas?  
    Em caso de falhas, favor reportar ao seguinte e-mail: **hugo_nc@outlook.com** ou **paulonsoares@yahoo.com.br**.

# **Licença**
Esta aplicação é livre,tendo seu maior enfoque no âmbito didático para disciplinas de programação Orientada a Objetos.
   		        